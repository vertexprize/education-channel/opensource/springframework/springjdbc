package org.vertexprize.education.spring.jdbcexample;

import jakarta.annotation.PostConstruct;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.vertexprize.education.spring.jdbcexample.entity.Student;
import org.vertexprize.education.spring.jdbcexample.service.DatabaseService;

@SpringBootApplication
public class JdbcExampleApplication {

    private static final Logger log = LoggerFactory.getLogger(JdbcExampleApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(JdbcExampleApplication.class, args);
    }

    @Autowired
    DatabaseService databaseService;

    @PostConstruct
    public void start() {
        log.info("Подсчет количества студентов в базе данных ...");
        databaseService.countStundens();

        log.info("Получение полного списка студентов в базе  ...");
        List<Student> stundents = databaseService.getAllStundents();

        if (!stundents.isEmpty()) {
            stundents.stream().forEach(stundent -> {
                log.info(stundent.toString());
            });
        }

        log.info("Добавление студентов ... ");

        
        Student student1 = new Student();
        student1.setFirstName("Дмитрий");
        student1.setMiddleName("Романович");
        student1.setSureName("Татарчук");
        student1.setStudentGroup("ПГС-21");
        student1.setYear(5);
        
       
        
        
        Student student2 = new Student();
        student2.setFirstName("Иван");
        student2.setMiddleName("Иванович");
        student2.setSureName("Иванович");
        student2.setStudentGroup("ПГС-21");
        student2.setYear(5);

        
        Student student3 = new Student();
        student3.setFirstName("Кирилл");
        student3.setMiddleName("Романович");
        student3.setSureName("Кичигин");
        student3.setStudentGroup("ПГС-21");
        student3.setYear(5);
        

        databaseService.saveStudent(student1);
        databaseService.saveStudent(student2);
        databaseService.saveStudent(student3);

        log.info("Получение полного списка студентов в базе  ...");
        stundents = databaseService.getAllStundents();

        if (!stundents.isEmpty()) {
            stundents.stream().forEach(stundent -> {
                log.info(stundent.toString());
            });
        }
        
        log.info("Удаление студента с идентификатором [2]");
        databaseService.deleteStudentById(2L);
        
        log.info("Получение полного списка студентов в базе  ...");
        stundents = databaseService.getAllStundents();

        if (!stundents.isEmpty()) {
            stundents.stream().forEach(stundent -> {
                log.info(stundent.toString());
            });
        }
        

         databaseService.deleteStudentById(100L);
        
    }

}
