drop table if exists STUDENT;


CREATE TABLE STUDENT
(
	ID integer GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	FIRST_NAME varchar(255),
        SURE_NAME varchar(255),	
        MIDDLE_NAME varchar(255),      
        STUDENT_GROUP varchar(255),
        YEAR_STUDY int          
);
