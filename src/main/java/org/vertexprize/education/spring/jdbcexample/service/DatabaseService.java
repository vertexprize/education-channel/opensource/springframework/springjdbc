/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.jdbcexample.service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.vertexprize.education.spring.jdbcexample.entity.Student;
import org.vertexprize.education.spring.jdbcexample.entity.StudentMapper;

/**
 *
 * @author vaganovdv
 */
@Service
public class DatabaseService {

    private static final Logger log = LoggerFactory.getLogger(DatabaseService.class);

    /**
     * Подключение класса JdbcTemplate
     */
    private final JdbcTemplate jdbcTemplate;

    public DatabaseService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Подсчет количества студентов
     *
     * @return
     */
    public long countStundens() {
        String sql = "SELECT COUNT (*) FROM STUDENT";
        long count = jdbcTemplate.queryForObject(sql, Long.class);
        log.info("Количество студентов в базе данных: [" + count + "] записей");
        return count;
    }

    /**
     * Получение полного списка студентов
     *
     * @return
     */
    public List<Student> getAllStundents() {
        String sql = "SELECT * FROM STUDENT";
        List<Student> students = jdbcTemplate.query(sql, new StudentMapper());
        log.info("Получен список студентов: [" + students + "] записей");
        return students;
    }

    /**
     * Сохранение единичной записи о студенте
     *
     * @param student
     */
    public void saveStudent(Student student) {
        String sql = "INSERT INTO STUDENT (FIRST_NAME, SURE_NAME, MIDDLE_NAME, STUDENT_GROUP, YEAR_STUDY) VALUES (?,?,?,?,?)";
        int update = jdbcTemplate.update(sql, student.getFirstName(), student.getSureName(), student.getMiddleName(), student.getStudentGroup(), student.getYear());
        log.info("Добавлена запись о студенте " + student.toString());
    }

    /**
     * Поиск студентов по фамилиии
     *
     * @param sureName
     * @return
     */
    public List<Student> findStudentBySureName(String sureName) {
        String sql = "SELECT * FROM student WHERE sure_name = '" + sureName + "' ";
        List<Student> students = jdbcTemplate.query(sql, new StudentMapper());
        log.info("Найдено [" + students.size() + "] записей сстудетов с фамилией [" + sureName + "]");
        return students;
    }

    /**
     * Поиск студентов по отчеству
     *
     * @param middleName
     * @return
     */
    public List<Student> findStudentByMiddleName(String middleName) {
        String sql = "SELECT * FROM student WHERE middle_name =  = '" + middleName + "' ";
        List<Student> students = jdbcTemplate.query(sql, new StudentMapper());
        log.info("Найдено [" + students.size() + "] записей студентов с отчеством [" + middleName + "]");
        return students;
    }

    /**
     * Удаление студента по идентификатору
     *
     * @param middleName
     * @return
     */
    public int deleteStudentById(Long id) {        
        String sql = "DELETE FROM student WHERE id =" + id + " ";
        int result = 0;        
        try {
            result = jdbcTemplate.update(sql);           
            if (result != 0) {
                log.info("Удалено [" + result + "] записей студента с идентификатором [" + id + "]");                
            } else {
                log.error("Ошибка удаления записи о студенте с идентификатором [" + id + "]");
            }            
        } catch (DataAccessException ex) {
            log.error("Ошибка удаления записи с идентификатором [" + id + "]: " + ex.toString());
        }        
        return result;
    }

}
