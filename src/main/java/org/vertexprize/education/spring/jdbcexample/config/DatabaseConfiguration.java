/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.jdbcexample.config;

import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author vaganovdv
 */
@Configuration
public class DatabaseConfiguration {
    
    private static final Logger log = LoggerFactory.getLogger(DatabaseConfiguration.class);
    
    /**
     * Местоположение базы данных Postgres
     * Пример: 
     * 
     * протокол  драйвер   ip адрес (узел) 
     *  |         /         /      порт приложения 
     *  |        |          |        |    имя базы данных
     *  |        |          |        |      / 
     * jdbc:postgresql://localhost:9432/postgres
     * 
     * 
     */
    @Value( "${spring.datasource.url}" )
    private String url;
    
    
    /**
     * Имя драйвера: org.postgresql.Driver
     * 
     */
    @Value( "${spring.datasource.driverClassName}" )
    private String driverClassName;
    
    
    /**
     * Имя пользователя базы данных Postres
     */
    @Value( "${spring.datasource.username}" )
    private String username;
    
    
    
    /**
     * Пароль пользователя базы данных Postres
     */
    @Value( "${spring.datasource.password}" )
    private String password;
    
    
    @Bean
    public DataSource dataSourceInit() {
        
        log.info("Настройка базы данных Postgres");
        log.info("database url: "+url);
        log.info("database driver: "+driverClassName);
        
        DriverManagerDataSource dataSource = new DriverManagerDataSource();                
        dataSource.setUrl(url);
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUsername(username);
        dataSource.setPassword(password);        
        return dataSource;
    }
    
}
