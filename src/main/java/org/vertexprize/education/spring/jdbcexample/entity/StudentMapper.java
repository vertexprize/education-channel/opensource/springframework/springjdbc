/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.jdbcexample.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author vaganovdv
 */
public class StudentMapper implements RowMapper< Student>{

    @Override
    public Student mapRow(ResultSet rs, int rowNum) throws SQLException {

        Student student = new Student();
        student.setId(rs.getInt("id"));
        student.setFirstName(rs.getString("first_name"));
        student.setSureName(rs.getString("sure_name"));
        student.setMiddleName(rs.getString("middle_name"));
        student.setStudentGroup(rs.getString("student_group"));
        student.setYear(rs.getInt("year_study"));
        return student;
    }
    
}
